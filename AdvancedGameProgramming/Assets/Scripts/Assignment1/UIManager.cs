﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {


	bool _isExtracting = GameManager.isExtracting;

	public Text extractionButton;
	public Text scoreText;
	public Text extractiontext;
	public Text scanText;

	public Text maxText;
	public Text halfText;
	public Text quarterText;
	public Text eighthText;
	public Text sixteenthText;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		scoreText.text = "Score: " + GameManager.score;
		extractiontext.text = "Extractions Remaining: " + GameManager.GetExtractionsRemaining();
		scanText.text = "Scans \nRemaining: " + GameManager.GetScansRemaining();
		maxText.text = ":" + GameManager.maxBlockValueStatic;
		halfText.text = ":" + GameManager.maxBlockValueStatic * 0.5f;
		quarterText.text = ":" + GameManager.maxBlockValueStatic * 0.25f;
		eighthText.text = ":" + GameManager.maxBlockValueStatic * 0.125f;
		sixteenthText.text = ":" + GameManager.maxBlockValueStatic * 0.0625f;
	}

	public void ToggleMode()
	{
		_isExtracting = !_isExtracting;
		GameManager.isExtracting = _isExtracting;

		if (_isExtracting)
		{
			extractionButton.text = "Mode: Extracting";
		}
		else
		{
			extractionButton.text = "Mode: Scanning";
		}
	}
}
