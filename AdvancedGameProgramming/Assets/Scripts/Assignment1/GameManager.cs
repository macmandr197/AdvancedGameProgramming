﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public enum TileDirection
    {
        NW,
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W
    };



//Grid Variables
    [Tooltip("set total number of columns to generate")]
    public int maxColumns = 4;
    [Tooltip("set total number of rows to generate")]
    public int maxRows = 4;
    private int blockArraySize;
    public GameObject blockObject;
    public static List<GameObject> blockList;
    [Tooltip("total amount of 'max' profitable blocks on grid")]
    public int maxFullBlockCount = 1; //total amount of 'max' profitable blocks on grid
    public int[] maxBlockArray; //array containing randomized index's for the block array (to avoid having the same block)
    TileDirection tileDir;

//UI variables
    public static bool isExtracting { get; set; }
    public static float score { get; set; }
    private static int  extractionsRemaining = 3;
    private static int scansRemaining = 6;

//Block variables
    public static int maxBlockValueStatic;
    public int maxBlockValue = 1000;

//Game variables
    bool isGameOver;
    bool hasSpawnedGameOverScreen;
    public GameObject GameOverScreen;
    GameObject gOScreen;

    // Use this for initialization
    [UsedImplicitly]
    private void Awake ()
    {
        maxBlockValueStatic = maxBlockValue;
        Initialize();
	}

    public void Initialize()
    {
        maxBlockArray = null;
        blockList = null;
        isExtracting = false;
        extractionsRemaining = 3;
        scansRemaining = 6;
        score = 0;
        blockList = new List<GameObject>();
        blockArraySize = maxColumns * maxRows;
        maxBlockArray = new int[maxFullBlockCount];
        GenerateGrid();
        GenerateMaxBlocks();
        GenerateAdjacentBlocks(); 
        isGameOver = false;
        hasSpawnedGameOverScreen = false;
        Destroy(gOScreen);            
    }

    private void GenerateGrid()
    {
        GameObject BlockGrid = new GameObject(); //creating a container for the grid of cubes
        BlockGrid.name = "BlockGrid";
        for (int i = 0, y = 0; y < maxRows; y++) //y axis
        {
            for (int x = 0; x < maxColumns; x++, i++) //x axis
            {
                GameObject cube = Instantiate(blockObject, new Vector3(blockObject.gameObject.transform.localScale.x *  x / (maxColumns/16), blockObject.gameObject.transform.localScale.y * -y / (maxRows/16), 0f), Quaternion.identity);
                cube.transform.localScale = new Vector3(cube.transform.localScale.x / (maxColumns/16), cube.transform.localScale.y / (maxRows/16),cube.transform.localScale.z);
                cube.name = (x) + ", " + (y) + " Index: " + i;
                cube.transform.parent = BlockGrid.transform;                
                blockList.Add(cube);
            }
        }
    }

    private void GenerateMaxBlocks()
    {
        for (int i = 0; i < maxFullBlockCount; i++)
        {
            int randomInt = Random.Range(0, blockArraySize - 1);
            while (maxBlockArray.Contains(randomInt))
            {
                //randomize until it doesn't contain the same number
                randomInt = Random.Range(0, blockArraySize - 1);
                Debug.LogError("Array contains number, re-randomizing");
            }
            maxBlockArray[i] = randomInt;
            blockList[maxBlockArray[i]].GetComponent<BlockScript>().CurrentBlockType = BlockScript.BlockType.Full;
            blockList[maxBlockArray[i]].GetComponent<BlockScript>().hasBeenAssigned = true;
        }
    }

    private void GenerateAdjacentBlocks() //function to check around a specified index
    {
        for (int i = 0; i < maxFullBlockCount; i++)
        {
            foreach(GameObject obj in blockList) 
            {
                if (Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) == 0)
                {
                    obj.GetComponent<BlockScript>().hasBeenAssigned = true;
                    if (obj.GetComponent<BlockScript>().CurrentBlockType > (BlockScript.BlockType)0)
                    {
                      obj.GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)0;   
                    }
                    
                }
                else if (Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) <=1.5f)
                {
                    obj.GetComponent<BlockScript>().hasBeenAssigned = true;
                    if (obj.GetComponent<BlockScript>().CurrentBlockType > (BlockScript.BlockType)1)
                    {
                        obj.GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)1; 
                    }
                    
                }
                else if (Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) >=1.5f && 
                        Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) <=2.85f)
                {
                    obj.GetComponent<BlockScript>().hasBeenAssigned = true;
                    if (obj.GetComponent<BlockScript>().CurrentBlockType > (BlockScript.BlockType)2)
                    {
                        obj.GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)2; 
                    }
                }
                /*else if (Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) >=2.85f && 
                        Vector3.Distance(blockList[maxBlockArray[i]].transform.position,obj.transform.position) <=4.25f)
                {
                    obj.GetComponent<BlockScript>().hasBeenAssigned = true;
                    if (obj.GetComponent<BlockScript>().CurrentBlockType > (BlockScript.BlockType)3)
                    {
                        obj.GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)3; 
                    }
                    
                }*/
            } 
        }


        
    }

    /*private int CheckAdjacency(int idx, TileDirection dir, int offset = 1) //deprecated
    {
        int val = 0;
        Vector2Int tempVec = indextoVector2Int(idx, gridWidth);
        switch (dir)
        {
            case TileDirection.E:
                if (tempVec.x < gridWidth-offset)
                {
                    tempVec.x += offset;
                    val = VectorToInt(tempVec);
                    if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;                        
                        
                    }

                }
                break;
            case TileDirection.W:
                if (tempVec.x > -1+offset)
                {
                    tempVec.x -= offset;
                    val = VectorToInt(tempVec);
                    if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                }
                break;
            case TileDirection.N:
                if (tempVec.y > -1+offset)
                {
                    tempVec.y -= offset;
                    val = VectorToInt(tempVec);
                    if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                }
                break;
            case TileDirection.S:
                if (tempVec.y < gridHeight - offset)
                {
                    tempVec.y += offset;
                    val = VectorToInt(tempVec);
                    if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                }
                break;
            case TileDirection.NE:
                if (tempVec.y > -1+offset && tempVec.x < gridWidth-offset)
                {
                    tempVec.y -= offset;
                    tempVec.x +=offset;
                    val = VectorToInt(tempVec);
                    if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                }
                break;
            case TileDirection.NW:
                if (tempVec.y > -1+offset && tempVec.x > -1+offset)
                    {
                        tempVec.y -= offset;
                        tempVec.x -=offset;
                        val = VectorToInt(tempVec);
                        if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                    }
                break;
            case TileDirection.SE:
                if (tempVec.y < gridHeight - offset && tempVec.x < gridWidth - offset)
                        {
                            tempVec.y += offset;
                            tempVec.x +=offset;
                            val = VectorToInt(tempVec);
                            if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                        }
                break;
            case TileDirection.SW:
                if (tempVec.y < gridHeight - offset && tempVec.x > -1+offset)
                        {
                            tempVec.y += offset;
                            tempVec.x -=offset;
                            val = VectorToInt(tempVec);
                            if (!blockList[val].GetComponent<BlockScript>().hasBeenAssigned)
                    {
                        blockList[val].GetComponent<BlockScript>().hasBeenAssigned = true;
                        blockList[val].GetComponent<BlockScript>().CurrentBlockType = (BlockScript.BlockType)offset;
                    }
                        }
                break;
        }
        return val;
    }*/

    public static int GetScansRemaining()
    {return scansRemaining;}

    public static int GetExtractionsRemaining()
    {
        return extractionsRemaining;
    }

    public static void DecrementExtractions()
    {
        if (extractionsRemaining > 0)
        {
            extractionsRemaining-=1;
        }
    }

    public static void DecrementScans()
    {
        if (scansRemaining > 0)
        {
            scansRemaining -=1;
        }
    }

    private int VectorToInt(Vector2Int vector)
    {
        return ((vector.y * maxRows) + vector.x);
    }

    private Vector2Int indextoVector2Int(int idx, int maxColSize)
    {

        return new Vector2Int(idx % maxColSize, idx / maxColSize);
    }

	// Update is called once per frame
	void Update ()
	{
        if (extractionsRemaining == 0)
        {
            isGameOver = true;
        }
         if (isGameOver)
         {
             if (!hasSpawnedGameOverScreen)
             {
                 GameObject tempObj = GameObject.Find("Canvas");
                 gOScreen = Instantiate(GameOverScreen);
                 gOScreen.transform.Find("finalScoreText").GetComponent<Text>().text = "Final Score: " + score;
                 gOScreen.transform.SetParent(tempObj.transform, false);
                 hasSpawnedGameOverScreen = true;

             }
         }   
	}

    
}
