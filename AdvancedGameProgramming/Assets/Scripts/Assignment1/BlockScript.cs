﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BlockScript : MonoBehaviour
{
    public enum BlockType
    {
        Full, Half, Quarter, Eighth, Sixteenth
    };

    public bool hasBeenAssigned = false;



    float pointValue;
    Color blockColor;
   

   [SerializeField]
    public BlockType CurrentBlockType { get; set; }


    // Use this for initialization
    void Awake ()
    {
        CurrentBlockType = BlockType.Sixteenth;
		GetComponent<Renderer>().material.shader = Shader.Find("Diffuse");
        
	}

    void Start()
    {
        InitializePoints();
        SetBlockColor();
        GetComponent<Renderer>().material.color = new Color32(193,193,193,255);
        //GetComponent<Renderer>().material.color = blockColor;// for debugging purposes (displays the current block color to mitigate having to scan them)
    }
	
	// Update is called once per frame
	void Update ()
    {
	}

    void OnMouseOver()
    {
        GetComponent<Renderer>().material.shader = Shader.Find("Outlined/Silhouetted Diffuse");
        //Debug.Log(CurrentBlockType);
    }

    void OnMouseExit()
    {
        GetComponent<Renderer>().material.shader = Shader.Find("Diffuse");
    }

    void InitializePoints()
    {
        switch (CurrentBlockType)
        {
            case BlockType.Full:
            pointValue = GameManager.maxBlockValueStatic;
            break;
            case BlockType.Half:
            pointValue = GameManager.maxBlockValueStatic * 0.5f;
            break;
            case BlockType.Quarter:
            pointValue = GameManager.maxBlockValueStatic * 0.25f;
            break;
            case BlockType.Eighth:
            pointValue = GameManager.maxBlockValueStatic * 0.125f;
            break;
            default:
            pointValue = GameManager.maxBlockValueStatic * 0.0625f;
            break;
        }
    }

    void OnMouseDown()
    {
        if (GameManager.isExtracting && GameManager.GetExtractionsRemaining() > 0)
        {
            GameManager.score += pointValue;
            

            foreach(GameObject block in GameManager.blockList)
            {
                if (Vector3.Distance(gameObject.transform.position,block.transform.position) == 0)
                {
                    CurrentBlockType = (BlockType)4;  
                    SetBlockColor();
                    gameObject.GetComponent<Renderer>().material.color = blockColor;                  
                }
                if (Vector3.Distance(gameObject.transform.position,block.transform.position) <=1.5f)
                {
                    if ((int)block.gameObject.GetComponent<BlockScript>().CurrentBlockType < 2) //two is the second to minimal block color
                    {
                        int tempInt = (int)block.gameObject.GetComponent<BlockScript>().CurrentBlockType;
                        tempInt+=1;
                        block.gameObject.GetComponent<BlockScript>().CurrentBlockType = (BlockType)tempInt;
                        block.gameObject.GetComponent<BlockScript>().SetBlockColor();
                        block.gameObject.GetComponent<BlockScript>().InitializePoints();
                        block.GetComponent<Renderer>().material.color = block.gameObject.GetComponent<BlockScript>().blockColor;
                    }
                    
                }
            }
            Debug.Log(pointValue);
            GameManager.DecrementExtractions();
            
        }
        if (!GameManager.isExtracting && GameManager.GetScansRemaining() > 0)
        {
                foreach(GameObject block in GameManager.blockList)
            {
                if (Vector3.Distance(gameObject.transform.position,block.transform.position) <=1.5f)
                {
                    block.gameObject.GetComponent<Renderer>().material.color = block.gameObject.GetComponent<BlockScript>().blockColor;                    
                }
            }
                GameManager.DecrementScans();
        }
    }

    void SetBlockColor()
    {
        switch (CurrentBlockType)
        {
            case BlockType.Full:
            blockColor = new Color32(255,0,0,255);
            break;
            case BlockType.Half:
            blockColor = new Color32(244,149,66,255);
            break;
            case BlockType.Quarter:
            blockColor = new Color32(69,244,66,255);
            break;
            case BlockType.Eighth:
            blockColor = new Color32(215,244,66,255);
            break;
            case BlockType.Sixteenth:
            blockColor = new Color32(193,193,193,255);
            break;
        }
    }

    

    public bool getHasBeenAssigned()
    {
        return hasBeenAssigned;
    }

}
