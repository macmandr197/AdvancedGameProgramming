﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour {

	GameObject gm;

	void Awake()
	{
		gm = GameObject.Find("GameManager");
	}

	public void RestartLevel()
    {
			Destroy(GameObject.Find("BlockGrid"));
			GameObject.Find("ModeButtonText").GetComponent<Text>().text = "Mode: Scanning";
			gm.GetComponent<GameManager>().Initialize();			
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
